package com.tutorialacademy.rest.utils;

/**
 * @author David Bernal on 15/06/2016.
 */
public class ArrayUtils {

    /**
     * Metodo que inicializa un arreglo con valores a 0
     *
     * @param array     arreglo a inicializar
     * @param dimension dimensiones que tendra el arreglo
     * @return {@link Integer[][][]} arreglo inicializado a 0
     */
    public static Integer[][][] initArray(Integer[][][] array, final Integer dimension) {

        if (array == null)
            array = new Integer[dimension][dimension][dimension];

        for (final Integer[][] arrayX : array) {
            for (final Integer[] arrayY : arrayX) {
                for (int arrayZ = 0; arrayZ < arrayY.length; arrayZ++) {
                    if (arrayY[arrayZ] == null)
                        arrayY[arrayZ] = 0;
                    break;
                }
            }
        }
        return array;
    }
}
