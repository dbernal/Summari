package com.tutorialacademy.rest.business.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * @author David Bernal on 15/06/2016.
 */
public class DTOIntResultOperation {


    private List<String> resultList;

    public DTOIntResultOperation() {
        setResultList(new ArrayList<String>());
    }

    public List<String> getResultList() {
        return resultList;
    }

    public void setResultList(List<String> resultList) {
        this.resultList = resultList;
    }

}
