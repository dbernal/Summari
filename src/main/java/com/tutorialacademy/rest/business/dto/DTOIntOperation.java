package com.tutorialacademy.rest.business.dto;

import java.util.List;

/**
 * @author David Bernal on 15/06/2016.
 */
public class DTOIntOperation {


    private List<DTOIntOperations> operations;
    private Integer sizeArray;

    public DTOIntOperation() {
    }


    public Integer getSizeArray() {
        return sizeArray;
    }

    public void setSizeArray(Integer sizeArray) {
        this.sizeArray = sizeArray;
    }

    public List<DTOIntOperations> getOperations() {
        return operations;
    }

    public void setOperations(List<DTOIntOperations> operations) {
        this.operations = operations;
    }
}
