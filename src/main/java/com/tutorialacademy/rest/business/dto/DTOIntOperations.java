package com.tutorialacademy.rest.business.dto;

/**
 * @author David Bernal on 15/06/2016.
 */
public class DTOIntOperations {


    private String operationValue;
    private String operationType;

    public DTOIntOperations() {
    }

    public String getOperationValue() {
        return operationValue;
    }

    public void setOperationValue(String operationValue) {
        this.operationValue = operationValue;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }
}
