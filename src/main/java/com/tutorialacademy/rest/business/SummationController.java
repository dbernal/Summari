package com.tutorialacademy.rest.business;


import com.tutorialacademy.rest.business.dto.DTOIntCube;
import com.tutorialacademy.rest.business.dto.DTOIntOperation;
import com.tutorialacademy.rest.business.dto.DTOIntOperations;
import com.tutorialacademy.rest.business.dto.DTOIntResultOperation;
import com.tutorialacademy.rest.utils.ArrayUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author David Bernal on 15/06/2016.
 */
public class SummationController {

    private static final String U = "U";
    private static final String Q = "Q";
    private static final Integer MAX_LIMIT = 100;
    private static final String NEW_LINE = "\n";

    /**
     * function that calculates the consultations or querys
     *
     * @param operationsList list with operations and suboperations to calculate
     * @return {@link java.util.List}
     */
    public List<DTOIntResultOperation> calculate(final List<DTOIntOperation> operationsList) {

        final List<DTOIntResultOperation> resultOperations = new ArrayList<DTOIntResultOperation>();

        DTOIntResultOperation resultOperation = null;

        //Iteration operations
        for (final DTOIntOperation item : operationsList) {

            Integer[][][] array = null;
            resultOperation = new DTOIntResultOperation();

            //array size
            final Integer definedArray = item.getSizeArray();

            //suboperacion to calculate (U Update / Q Query)");
            for (final DTOIntOperations operations : item.getOperations()) {

                final String operation = operations.getOperationType();

                final Integer dimension = definedArray;

                if (dimension > MAX_LIMIT)
                    break;

                //Inicialisation of the array
                array = ArrayUtils.initArray(array, dimension);

                DTOIntCube cube = null;

                if (U.equalsIgnoreCase(operation)) {

                    //Enter the update data
                    final String update = operations.getOperationValue();

                    final String[] items = update.split(" ");

                    final Integer x = Integer.valueOf(items[0]) - 1;
                    final Integer y = Integer.valueOf(items[1]) - 1;
                    final Integer z = Integer.valueOf(items[2]) - 1;
                    final Integer w = Integer.valueOf(items[3]);

                    cube = new DTOIntCube(x, y, x);
                    //Validate range of the dimensions
                    if (cube.getX() > dimension || cube.getY() > dimension || cube.getZ() > dimension)
                        break;

                    //Update array index
                    array[cube.getX()][cube.getY()][cube.getZ()] = w;

                    //add output each operation
                    resultOperation.getResultList().add(array[cube.getX()][cube.getY()][cube.getZ()] + NEW_LINE);
                    System.out.println(U + " " + array[x][y][z]);
                } else {
                    //Enter the data to query
                    final String query = operations.getOperationValue();

                    final String[] split = query.split(" ");

                    final Integer x1 = Integer.valueOf(split[0]) - 1;
                    final Integer y1 = Integer.valueOf(split[1]) - 1;
                    final Integer z1 = Integer.valueOf(split[2]) - 1;
                    final Integer x2 = Integer.valueOf(split[3]);
                    final Integer y2 = Integer.valueOf(split[4]);
                    final Integer z2 = Integer.valueOf(split[5]);

                    Integer total = 0;

                    cube = new DTOIntCube(x1, y1, z1);

                    //validate start position and end position to de array
                    if ((cube.getX() >= x2) || (cube.getY() >= y2) || cube.getZ() >= z2)
                        break;

                    for (int i = x2 - 1; i >= x1; i--)
                        total += array[i][i][i];

                    System.out.println(Q + " " + total);
                    //add output each operation
                    resultOperation.getResultList().add(total + NEW_LINE);
                }
            }
            resultOperations.add(resultOperation);
        }
        return resultOperations;
    }
}
