package com.tutorialacademy.rest.comtroller;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Clase encargada de proporcionar el contecto de la aplicacion
 * @author David Bernal on 15/06/2016.
 */
@ApplicationPath("app")
public class RestConfig extends Application {
}
