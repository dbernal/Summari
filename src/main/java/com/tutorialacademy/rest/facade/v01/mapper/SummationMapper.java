package com.tutorialacademy.rest.facade.v01.mapper;

import com.tutorialacademy.rest.business.dto.DTOIntOperation;
import com.tutorialacademy.rest.business.dto.DTOIntOperations;
import com.tutorialacademy.rest.business.dto.DTOIntResultOperation;
import com.tutorialacademy.rest.facade.v01.dto.Operation;
import com.tutorialacademy.rest.facade.v01.dto.Operations;
import com.tutorialacademy.rest.facade.v01.dto.ResultOperation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Entelgy on 20/06/2016.
 */
public class SummationMapper {


    /**
     * Method which maps a external object to internal object
     *
     * @param operationList
     * @return {@link java.util.List}
     */
    public List<DTOIntOperation> mapInOperationToDTOIntOperation(final List<Operation> operationList) {

        final List<DTOIntOperation> dtoIntOperations = new ArrayList<DTOIntOperation>();

        for (final Operation operation : operationList) {

            final List<DTOIntOperations> operationsList = new ArrayList<DTOIntOperations>();

            final DTOIntOperation dtoIntOperation = new DTOIntOperation();
            dtoIntOperation.setSizeArray(operation.getSizeArray());

            for (final Operations operations : operation.getOperations()) {
                final DTOIntOperations intOperations = new DTOIntOperations();
                intOperations.setOperationType(operations.getOperationType());
                intOperations.setOperationValue(operations.getOperationValue());
                operationsList.add(intOperations);
            }
            dtoIntOperation.setOperations(operationsList);
            dtoIntOperations.add(dtoIntOperation);
        }


        return dtoIntOperations;
    }

    /**
     * Method which maps a internal object to external object
     *
     * @param intResultOperations
     * @return {@link java.util.List}
     */
    public List<ResultOperation> mapOutDTOIntResultOperationToResultOperation(final List<DTOIntResultOperation> intResultOperations) {

        final List<ResultOperation> resultOperations = new ArrayList<ResultOperation>();

        for (final DTOIntResultOperation intResultOperation : intResultOperations) {
            final ResultOperation operation = new ResultOperation();
            operation.setResultList(intResultOperation.getResultList());

            resultOperations.add(operation);
        }
        return resultOperations;
    }
}
