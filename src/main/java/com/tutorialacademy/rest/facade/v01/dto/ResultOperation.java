package com.tutorialacademy.rest.facade.v01.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * @author David Bernal on 15/06/2016.
 */
public class ResultOperation {

    //Atributo que contendra    el resultado por cada listado de consultas
    private List<String> resultList;

    public ResultOperation() {
        setResultList(new ArrayList<String>());
    }

    public List<String> getResultList() {
        return resultList;
    }

    public void setResultList(List<String> resultList) {
        this.resultList = resultList;
    }

}
