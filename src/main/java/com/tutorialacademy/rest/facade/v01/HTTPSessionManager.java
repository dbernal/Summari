package com.tutorialacademy.rest.facade.v01;

import com.tutorialacademy.rest.business.SummationController;
import com.tutorialacademy.rest.business.dto.DTOIntOperation;
import com.tutorialacademy.rest.facade.v01.dto.Operation;
import com.tutorialacademy.rest.facade.v01.dto.ResultOperation;
import com.tutorialacademy.rest.facade.v01.mapper.SummationMapper;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * @author David Bernal on 15/06/2016.
 */
@Path("/rest")
public class HTTPSessionManager {

    /**
     * @param operationList Listado con el numero de operaciones a realizar, junto con sus sub operaciones
     * @return {@link java.util.List}
     */
    @GET
    @Path("/")
    @Produces({MediaType.APPLICATION_JSON})
    public List<ResultOperation> cubeSummation(final List<Operation> operationList) {

        final SummationMapper mapper = new SummationMapper();

        final List<DTOIntOperation> dtoIntOperations = mapper.mapInOperationToDTOIntOperation(operationList);

        final List<ResultOperation> operations = mapper.mapOutDTOIntResultOperationToResultOperation(
                new SummationController().calculate(dtoIntOperations)
        );
        return operations;
    }
}
