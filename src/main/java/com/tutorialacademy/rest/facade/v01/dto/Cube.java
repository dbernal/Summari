package com.tutorialacademy.rest.facade.v01.dto;

/**
 * Class cube representative
 * @author David Bernal on 18/06/2016.
 */

public class Cube {

    private Integer x;
    private Integer y;
    private Integer z;

    public Cube() {
    }

    public Cube(Integer x, Integer y, Integer z) {
        this.x = x;
        this.y = y;
        this.z = z;

    }

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }

    public Integer getZ() {
        return z;
    }

    public void setZ(Integer z) {
        this.z = z;
    }
}
