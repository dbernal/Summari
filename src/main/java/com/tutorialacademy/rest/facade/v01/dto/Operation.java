package com.tutorialacademy.rest.facade.v01.dto;

import java.io.Serializable;
import java.util.List;

/**
 * @author David Bernal on 15/06/2016.
 */
public class Operation implements Serializable {

    //Atributo que contendra el numero de operaciones a realizar
    private List<Operations> operations;
    //Atributo que indicada la dimension que tendra cada arreglo
    private Integer sizeArray;

    public Operation() {
    }


    public Integer getSizeArray() {
        return sizeArray;
    }

    public void setSizeArray(Integer sizeArray) {
        this.sizeArray = sizeArray;
    }

    public List<Operations> getOperations() {
        return operations;
    }

    public void setOperations(List<Operations> operations) {
        this.operations = operations;
    }
}
