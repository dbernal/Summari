package com.tutorialacademy.rest.facade.v01.dto;

import java.io.Serializable;

/**
 * @author David Bernal on 15/06/2016.
 */
public class Operations implements Serializable{

    //Atributo que contrendra el valor por cada operacion (Query (1 1 1 3 3 3)/Update( 2 2 2 44))
    private String operationValue;
    //Atributo que contrendra el tipo para cada operacion (Query/Update)
    private String operationType;

    public Operations() {
    }

    public String getOperationValue() {
        return operationValue;
    }

    public void setOperationValue(String operationValue) {
        this.operationValue = operationValue;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }
}
