package com.tutorialacademy.rest.facade.v01.mapper;

import com.tutorialacademy.rest.DummyMock;
import com.tutorialacademy.rest.business.dto.DTOIntOperation;
import com.tutorialacademy.rest.business.dto.DTOIntResultOperation;
import com.tutorialacademy.rest.facade.v01.dto.Operation;
import com.tutorialacademy.rest.facade.v01.dto.ResultOperation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class SummationMapperTest {

    private SummationMapper mapper;

    private DummyMock mock;

    @Before
    public void init() {
        mapper = new SummationMapper();
        mock = new DummyMock();
    }

    @Test
    public void testMapInOperationToDTOIntOperation() throws Exception {

        final List<Operation> operationsList = mock.getOperationsList();

        final List<DTOIntOperation> result = mapper.mapInOperationToDTOIntOperation(operationsList);

        Assert.assertNotNull(result);
        Assert.assertEquals(result.get(0).getSizeArray(), operationsList.get(0).getSizeArray());
        Assert.assertEquals(result.get(0).getOperations().get(0).getOperationType(), operationsList.get(0).getOperations().get(0).getOperationType());
        Assert.assertEquals(result.get(0).getOperations().get(0).getOperationValue(), operationsList.get(0).getOperations().get(0).getOperationValue());
    }

    @Test
    public void testMapOutDTOIntResultOperationToResultOperation() throws Exception {
        final List<DTOIntResultOperation> dtoIntResultOperationList = mock.getDTOIntResultOperationList();

        final List<ResultOperation> resultOperations = mapper.mapOutDTOIntResultOperationToResultOperation(dtoIntResultOperationList);

        Assert.assertNotNull(resultOperations);
        Assert.assertEquals(resultOperations.get(0).getResultList().get(0), dtoIntResultOperationList.get(0).getResultList().get(0));
    }
}