package com.tutorialacademy.rest.facade.v01;

import com.tutorialacademy.rest.DummyMock;
import com.tutorialacademy.rest.facade.v01.dto.Operation;
import com.tutorialacademy.rest.facade.v01.dto.ResultOperation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class HTTPSessionManagerTest {

    private HTTPSessionManager summation;

    private DummyMock mock;

    @Before
    public void init() {
        summation = new HTTPSessionManager();
        mock = new DummyMock();
    }

    @Test
    public void testCubeSummation() throws Exception {

        final List<Operation> operationsList = mock.getOperationsList();

        final List<ResultOperation> resultOperations = summation.cubeSummation(operationsList);

        Assert.assertNotNull(resultOperations);
    }
}