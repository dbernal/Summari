package com.tutorialacademy.rest.facade.v01;

import com.tutorialacademy.rest.facade.v01.dto.Operation;
import com.tutorialacademy.rest.facade.v01.dto.Operations;
import com.tutorialacademy.rest.facade.v01.dto.ResultOperation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Entelgy on 17/06/2016.
 */
public class Main {

    public static void main(String[] args) {

        final List<Operation> operationList = new ArrayList<Operation>();

        List<Operations> operationsList = new ArrayList<Operations>();

        Operations operations = new Operations();
        operations.setOperationType("U");
        operations.setOperationValue("2 2 2 44");

        operationsList.add(operations);

        operations = new Operations();
        operations.setOperationType("U");
        operations.setOperationValue("1 1 1 22");

        operationsList.add(operations);

        operations = new Operations();
        operations.setOperationType("Q");
        operations.setOperationValue("1 1 1 2 2 2");

        operationsList.add(operations);

        Operation operation = new Operation();
        operation.setSizeArray(2);
        operation.setOperations(operationsList);

        operationList.add(operation);

        //TODO nueva llamada
        operationsList = new ArrayList<Operations>();

        operations = new Operations();
        operations.setOperationType("U");
        operations.setOperationValue("2 2 2 44");

        operationsList.add(operations);

        operations = new Operations();
        operations.setOperationType("U");
        operations.setOperationValue("1 1 1 22");

        operationsList.add(operations);

        operations = new Operations();
        operations.setOperationType("Q");
        operations.setOperationValue("1 1 1 2 2 2");

        operationsList.add(operations);

        operation = new Operation();
        operation.setSizeArray(4);
        operation.setOperations(operationsList);

        operationList.add(operation);

        final List<ResultOperation> result = new HTTPSessionManager().cubeSummation(operationList);

        for (final ResultOperation resultOperation : result) {
            for (final String s : resultOperation.getResultList()) {
                System.out.print(s);
            }
        }



    }
}
