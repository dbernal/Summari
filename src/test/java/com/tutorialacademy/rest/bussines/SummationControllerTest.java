package com.tutorialacademy.rest.bussines;

import com.tutorialacademy.rest.DummyMock;
import com.tutorialacademy.rest.business.SummationController;

import com.tutorialacademy.rest.business.dto.DTOIntOperation;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class SummationControllerTest {

    private SummationController controller;

    private DummyMock mock;

    @Before
    public void setUp() throws Exception {
        controller = new SummationController();
        mock = new DummyMock();
    }

    @Test
    public void sizeArrayMaxLengthTest() {

        final List<DTOIntOperation> operationsList = mock.getDTOIntOperationsList();

        operationsList.get(0).setSizeArray(200);

        controller.calculate(operationsList);
    }

    @Test
    public void dimensionXMaxArraySizeUpdateTest() {

        final List<DTOIntOperation> operationsList = mock.getDTOIntOperationsList();

        operationsList.get(0).getOperations().get(0).setOperationValue("22 22 22 55");

        controller.calculate(operationsList);

    }

    @Test
    public void dimensionYMaxArraySizeUpdateTest() {

        final List<DTOIntOperation> operationsList = mock.getDTOIntOperationsList();

        operationsList.get(0).getOperations().get(0).setOperationValue("22 22 22 55");

        controller.calculate(operationsList);
    }

    @Test
    public void dimensionX1MaxX2QueryTest() {

        final List<DTOIntOperation> operationsList = mock.getDTOIntOperationsList();

        operationsList.get(0).getOperations().get(0).setOperationType("Q");
        operationsList.get(0).getOperations().get(0).setOperationValue("2 2 2 1 1 1");

        controller.calculate(operationsList);
    }

    @Test
    public void dimensionY2MaxY2QueryTest() {

        final List<DTOIntOperation> operationsList = mock.getDTOIntOperationsList();

        operationsList.get(0).getOperations().get(0).setOperationType("Q");
        operationsList.get(0).getOperations().get(0).setOperationValue("2 2 2 1 1 1");

        controller.calculate(operationsList);


    }

    @Test
    public void dimensionX2MaxX2QueryTest() {

        final List<DTOIntOperation> operationsList = mock.getDTOIntOperationsList();

        operationsList.get(0).getOperations().get(0).setOperationType("Q");
        operationsList.get(0).getOperations().get(0).setOperationValue("2 2 2 1 1 1");

        controller.calculate(operationsList);

    }


}