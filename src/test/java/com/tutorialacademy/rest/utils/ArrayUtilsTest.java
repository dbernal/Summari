package com.tutorialacademy.rest.utils;

import org.junit.Assert;
import org.junit.Test;

public class ArrayUtilsTest {

    @Test
    public void testInitArray() throws Exception {

        Integer[][][] array = null;

        final Integer[][][] integers = ArrayUtils.initArray(array, 3);

        Assert.assertEquals(integers.length, 3);
    }
}