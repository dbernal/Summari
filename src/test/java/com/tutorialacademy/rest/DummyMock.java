package com.tutorialacademy.rest;

import com.tutorialacademy.rest.business.dto.DTOIntOperation;
import com.tutorialacademy.rest.business.dto.DTOIntOperations;
import com.tutorialacademy.rest.business.dto.DTOIntResultOperation;
import com.tutorialacademy.rest.facade.v01.dto.Operation;
import com.tutorialacademy.rest.facade.v01.dto.Operations;

import java.util.ArrayList;
import java.util.List;

/**
 * Clase que retorna objetos previamente configurados
 *
 * @author David Bernal on 17/06/2016.
 */
public class DummyMock {

    public List<DTOIntOperation> getDTOIntOperationsList() {

        final List<DTOIntOperation> operationList = new ArrayList<DTOIntOperation>();

        List<DTOIntOperations> operationsList = new ArrayList<DTOIntOperations>();

        DTOIntOperations operations = new DTOIntOperations();
        operations.setOperationType("U");
        operations.setOperationValue("2 2 2 44");

        operationsList.add(operations);

        operations = new DTOIntOperations();
        operations.setOperationType("U");
        operations.setOperationValue("1 1 1 22");

        operationsList.add(operations);

        operations = new DTOIntOperations();
        operations.setOperationType("Q");
        operations.setOperationValue("1 1 1 2 2 2");

        operationsList.add(operations);

        DTOIntOperation operation = new DTOIntOperation();
        operation.setSizeArray(2);
        operation.setOperations(operationsList);

        operationList.add(operation);

        return operationList;
    }

    public List<Operation> getOperationsList() {

        final List<Operation> operationList = new ArrayList<Operation>();

        List<Operations> operationsList = new ArrayList<Operations>();

        Operations operations = new Operations();
        operations.setOperationType("U");
        operations.setOperationValue("2 2 2 44");

        operationsList.add(operations);

        operations = new Operations();
        operations.setOperationType("U");
        operations.setOperationValue("1 1 1 22");

        operationsList.add(operations);

        operations = new Operations();
        operations.setOperationType("Q");
        operations.setOperationValue("1 1 1 2 2 2");

        operationsList.add(operations);

        Operation operation = new Operation();
        operation.setSizeArray(2);
        operation.setOperations(operationsList);

        operationList.add(operation);

        return operationList;
    }

    public List<DTOIntResultOperation> getDTOIntResultOperationList() {

        final List<DTOIntResultOperation> dtoIntResultOperations = new ArrayList<DTOIntResultOperation>();

        final List<String> strings = new ArrayList<String>();
        strings.add("2");
        strings.add("4");
        strings.add("2");
        strings.add("27");
        strings.add("1");

        final DTOIntResultOperation dtoIntResultOperation = new DTOIntResultOperation();
        dtoIntResultOperation.setResultList(strings);

        dtoIntResultOperations.add(dtoIntResultOperation);

        return dtoIntResultOperations;
    }
}
